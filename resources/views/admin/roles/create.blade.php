@include('admin.layout.sidebar')


<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-lg-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('roles') }}">Roles</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a>create</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-lg-6 text-right">

        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 mx-auto">
            <form action="{{ url('roles') }}" id="roleForm" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label> Select Permission</label>
                    <div class="row">

                        <div class="col-lg-4 mb-4">
                            <h6>Products</h6>
                            @foreach ($permissions as $permission)
                                @if ($permission->permission_for == 'Products')
                                    <div>
                                        <label>
                                            <input type="checkbox" name="permission[]" value="{{ $permission->id }}">
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-lg-4 mb-4" >
                            <h6>Customers</h6>
                            @foreach ($permissions as $permission)
                                @if ($permission->permission_for == 'Customers')
                                    <div>
                                        <label>
                                            <input type="checkbox" name="permission[]" value="{{ $permission->id }}">
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-lg-4 mb-4">
                            <h6>Orders</h6>
                            @foreach ($permissions as $permission)
                                @if ($permission->permission_for == 'Orders')
                                    <div>
                                        <label>
                                            <input type="checkbox" name="permission[]" value="{{ $permission->id }}">
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <div class="col-lg-4 mb-4">
                            <h6>Other</h6>
                            @foreach ($permissions as $permission)
                                @if ($permission->permission_for == 'Other')
                                    <div>
                                        <label>
                                            <input type="checkbox" name="permission[]" value="{{ $permission->id }}">
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-sm nf-btnAdd" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>

@include('admin.layout.footer')
