@include('admin.layout.sidebar')


<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-lg-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><a >Permission</a></li>
                </ol>
              </nav>
        </div>
        <div class="col-lg-6 text-right">
            <div class="row mb-2">
                <div class="col-lg-12">
                    <a href="{{url('roles')}}" class="btn btn-sm btn-warning">Roles</a>
                    <a href="{{url('permission')}}" class="btn btn-sm btn-info ">Permission</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{url('permission/create')}}" class="btn btn-sm nf-btnAdd">Add</a>
                </div>
            </div>
            
        </div>
    </div>
@if(session('success'))
    <div class="row text-center">
        <div class="col-lg-12">
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Permission For</th>
                  </tr>
                </thead>
                <tbody>
                 @php($i=1)
                    @foreach ($permissions as $permission)
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->permission_for}}</td>
                        <td>
                            <a class="nf-btnEdit" href="{{url("permission/$permission->id/edit")}}"><i class="fas fa-edit"></i></a>
                        </td>
                      </tr>
                      @php($i++)
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>

@include('admin.layout.footer')