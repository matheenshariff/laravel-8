@include('admin.layout.sidebar')


<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-lg-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{url('permission')}}">Permission</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><a >create</a></li>
                </ol>
              </nav>
        </div>
        <div class="col-lg-6 text-right">
            
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 mx-auto">
            <form action="{{url('permission')}}" id="permissionForm" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <select name="permission_for" class="form-control">
                        <option disabled selected>Select</option>
                        <option>Products</option>
                        <option>Customers</option>
                        <option>Orders</option>
                        <option>Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label >Permission Name</label>
                    <input type="text" name="name" class="form-control" >
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-sm nf-btnAdd" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>

@include('admin.layout.footer')